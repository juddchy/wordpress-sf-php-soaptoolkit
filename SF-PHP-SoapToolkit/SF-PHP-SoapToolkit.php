<?php
/*
Plugin Name: Salesforce-PHP SOAP Toolkit
Plugin URI: http://www.salesforce.com
Description: Salesforce-PHP SOAP Toolkit (uses v. 27.02)
Version: 1.0
Author: Salesforce
License: GPL
*/
define('WP_DEBUG', true);  // turn off once testing complete
define('TOOLKIT_DIR', 'SalesforceToolkit');		// first dir of toolkit

/* Runs when plugin is activated */
//register_activation_hook(__FILE__,'salesforce_PHP_SoapToolkit_activate'); 
function salesforce_PHP_SoapToolkit_activate() {
	// load enterrpise client
	//$fullFileName = __DIR__ . DIRECTORY_SEPARATOR . TOOLKIT_DIR . DIRECTORY_SEPARATOR . 'soapclient'. DIRECTORY_SEPARATOR . 'SforceEnterpriseClient.php';	
	//echo '****' . $fullFileName;
//	if( is_file( $fullFileName) ){
//		require_once($fullFileName);
//		echo '**** it has been required';
//	}	
	
	//echo 'Salesforce PHP Toolkit activated !';
	return $fullFileName;
}

/* Runs on plugin deactivation*/
register_deactivation_hook( __FILE__, 'salesforce_PHP_SoapToolkit_deactivate' );
function salesforce_PHP_SoapToolkit_deactivate(){
	echo 'Salesforce PHP Toolkit deactivated !';
}
?>