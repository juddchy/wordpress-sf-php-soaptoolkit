<?php
  define('SALESFORCE_TOOLKIT_ROOT', WP_PLUGIN_DIR );
  define('SALESFORCE_TOOLKIT_NAME', 'SF-PHP-SoapToolkit' );
   
  $fileName  = SALESFORCE_TOOLKIT_ROOT . DIRECTORY_SEPARATOR . SALESFORCE_TOOLKIT_NAME . DIRECTORY_SEPARATOR . 'SalesforceToolkit' . DIRECTORY_SEPARATOR . 'soapclient' . DIRECTORY_SEPARATOR . 'SforceEnterpriseClient.php';
  if ( is_file( $fileName)){
	  echo 'requiring => ' . $fileName;
	require_once( $fileName);
  }

  
  function sforceSoapToolkitAutoloader( $className){

	echo '**in sforceSoapToolkitAutoloader ==> ' .'$class = ' . $className;
	
     //does this belong to this toolkit ? 
    if ( strpos($className, PUBLIC_INTERFACE_NAME ) === false ) {
		echo "this is it";
        return;
    }
	
    //replace backslashes from the namespace with a normal directory separator
    $file = sprintf('%s/%s.php', WP_PLUGIN_DIR, str_replace('\\', DIRECTORY_SEPARATOR, $className));

	echo 'file name => ' . $file;
    //include your file
    if (is_file($file)) {
        require_once($file);
    } else
		echo 'Cannot find this file => ' . $file . '  Has installation directory been renamed ?';
		
  }
  
?>