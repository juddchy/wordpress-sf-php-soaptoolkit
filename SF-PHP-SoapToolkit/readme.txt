=== Plugin Name ===
Contributors: Salesforce PHP developers
Donate link: http://www.salesforce.com/
Requires at least: PHP >= 5.0

Salesforce-PHP SOAP Toolkit

== Description ==
This is a wrapper plug in around the Salesforce SOAP Toolkit for PHP created by Salesforce. 
It wrap version 27.02 of toolkit

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload SF-PHP-SoapToolkit to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

`<?php code(); // goes in backticks ?>`